import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ������� on 14.06.2015.
 */
public class DataLoader {
    private File inputFile;
     List<String> strings = new ArrayList<>();
    public DataLoader(String fileName) {
        this.inputFile = new File(fileName);
    }

    public List<String> read() throws IOException {
        String outputLine = "";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)))) {
            String inputLine = "";

            while ((inputLine = reader.readLine()) != null) {
                strings.add(inputLine);
            }
        }
        return strings;
    }
}
