import java.util.LinkedList;
import java.util.List;

/**
 * Created by ������� on 16.06.2015.
 */
public class JsonParser {
    public String parse(List<String> input) {
        List<Record> records = new LinkedList<>();
        for (int i = 0;i <= input.size() - 1;i++) {
            Record record = new Record();
            String[] JsonAndValues = input.get(i).split("##");
            record.setValues(JsonAndValues[1]);

            String json = JsonAndValues[0];
            json = json.replace("{", "").replace("}", "").replace("\"","");
            for (String field : json.split(",")){
                String[] keyAndValue = field.split(":");
                System.out.println(keyAndValue[0]);
                if (keyAndValue[0] == "PeriodNum"){record.setPeriodNum(keyAndValue[1]);}
                if (keyAndValue[0] == "Year"){record.setYear(keyAndValue[1]);}
                if (keyAndValue[0] == "ValueType"){record.setValueType(keyAndValue[1]);}
                if (keyAndValue[0] == "OpCode22"){record.setOpCode22(keyAndValue[1]);}
                if (keyAndValue[0] == "OKPO"){record.setOkpo(keyAndValue[1]);}

            }

            records.add(record);
        }
        return records.toString();
    }

    private class Record {
        private String periodNum;
        private String year;
        private String valueType;
        private String opCode22;
        private String okpo;
        private String Values;

        public String getPeriodNum() {
            return periodNum;
        }

        public void setPeriodNum(String periodNum) {
            this.periodNum = periodNum;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getValueType() {
            return valueType;
        }

        public void setValueType(String valueType) {
            this.valueType = valueType;
        }

        public String getOpCode22() {
            return opCode22;
        }

        public void setOpCode22(String opCode22) {
            this.opCode22 = opCode22;
        }

        public String getOkpo() {
            return okpo;
        }

        public void setOkpo(String okpo) {
            this.okpo = okpo;
        }

        public String getValues() {
            return Values;
        }

        public void setValues(String values) {
            Values = values;
        }

        @Override
        public String toString() {
            return "Record{" +
                    "periodNum='" + periodNum + '\'' +
                    ", year='" + year + '\'' +
                    ", valueType='" + valueType + '\'' +
                    ", opCode22='" + opCode22 + '\'' +
                    ", okpo='" + okpo + '\'' +
                    ", Values='" + Values + '\'' +
                    '}';
        }
    }
}


