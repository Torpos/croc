package com.croc.edu;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Дмитрий on 09.07.2015.
 */
public interface Loader extends Closeable {
    boolean hasNext() throws IOException, SQLException;
    String getRecord() throws IOException, SQLException, ClassNotFoundException;
    String getHeaders() throws IOException, SQLException;
    String getNumber() throws SQLException;
}
