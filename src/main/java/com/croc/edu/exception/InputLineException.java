package com.croc.edu.exception;

/**
 * Created by Дмитрий on 24.07.2015.
 */
public class InputLineException extends Exception {
   public InputLineException(String message) {
        super(message);
    }
}
