package com.croc.edu;

import org.json.JSONObject;

/**
 * Created by Дмитрий on 12.07.2015.
 */
public class JsonParser {

    public String parse(String inputLine,String head){
        JSONObject jsonObject = new JSONObject(inputLine);
            if (jsonObject.has(head.trim())) {
                return jsonObject.getString(head.trim()) + "," ;
            }
            else {
                return "-," ;
            }
        }
    }

