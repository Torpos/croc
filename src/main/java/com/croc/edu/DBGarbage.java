package com.croc.edu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

/**
 * Created by Дмитрий on 14.07.2015.
 */
public class DBGarbage {
        public  void garbage() throws IOException, SQLException, ClassNotFoundException {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            try(Connection connection = DriverManager.getConnection("jdbc:oracle:thin:system@//localhost:1521/xe", "OPSTORE", "OPSTORE")) {

                PreparedStatement selectStatement = connection.prepareStatement("SELECT  DATA_KEY, VALUE_DATA FROM INT_DATA");
                ResultSet resultSet = selectStatement.executeQuery();
                Loader dataLoader = new DataLoader(connection,resultSet);
                JsonParser jsonParser = new JsonParser();
                Saver saver = new FileSaver("D:\\File\\db.txt");
                FileWriter fileWriter = new FileWriter("D:\\File\\db.txt",true);

                fileWriter.write(dataLoader.getHeaders() + ",Number");
                fileWriter.close();

                String[] headers = dataLoader.getHeaders().split(",") ;

                while (dataLoader.hasNext()) {

                    FileWriter addSeparator = new FileWriter("D:\\File\\db.txt", true);
                    addSeparator.write(System.lineSeparator());
                    addSeparator.close();

                    for (String head : headers) {
                        saver.save(jsonParser.parse(dataLoader.getRecord(), head));
                    }
                    saver.save(dataLoader.getNumber());
                }
            }

        }
}