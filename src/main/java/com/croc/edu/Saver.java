package com.croc.edu;

import java.io.IOException;

/**
 * Created by Дмитрий on 12.07.2015.
 */
public interface Saver extends Cloneable {
    void save(String inputLine) throws IOException;
}
