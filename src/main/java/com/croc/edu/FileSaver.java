package com.croc.edu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Дмитрий on 12.07.2015.
 */
public class FileSaver implements Saver {

    private File file;

    public FileSaver(String fileName) {
        File file = new File(fileName);
        this.file = file;
    }

    @Override
    public void save(String inputLine) throws IOException {
        FileWriter fileWriter = new FileWriter(file,true);
        fileWriter.write(inputLine);
        fileWriter.close();
    }
}
