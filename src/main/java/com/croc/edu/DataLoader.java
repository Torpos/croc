package com.croc.edu;

import org.json.JSONObject;

import java.sql.*;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Дмитрий on 14.07.2015.
 */
public class DataLoader implements Loader {
    private ResultSet resultSet;
    private PreparedStatement selectStatement;

    public DataLoader(Connection connection, ResultSet resultSet) throws SQLException {
        PreparedStatement selectStatement = connection.prepareStatement("SELECT  DATA_KEY, VALUE_DATA FROM INT_DATA");
        this.selectStatement = selectStatement;
        this.resultSet = resultSet;
    }

    @Override
    public boolean hasNext() throws IOException, SQLException {
        return resultSet.next();
    }

    @Override
    public String getRecord() throws IOException, SQLException, ClassNotFoundException {
            return resultSet.getString("DATA_KEY");
    }

    @Override
    public String getHeaders() throws IOException, SQLException {
        Set<String> headers = new LinkedHashSet<>();
        String inputLine;

            while ( hasNext()) {
                inputLine = resultSet.getString("DATA_KEY");
                JSONObject jsonObject = new JSONObject(inputLine);
                headers.addAll(jsonObject.keySet());
            }

        resultSet = selectStatement.executeQuery();

        return headers.toString().replace("[", " ").replace("]", " ").trim();

    }
    @Override
    public String getNumber() throws SQLException {
        return resultSet.getString("VALUE_DATA");
    }

    @Override
    public void close() throws IOException {

    }
}
