package com.croc.edu;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Дмитрий on 09.07.2015.
 */
public class FileLoader implements Loader  {
    private File inputFile;
    private BufferedReader reader;
    private String readLine;

    public FileLoader(File inputFile) throws IOException {
        this.inputFile = inputFile;
        reader = new BufferedReader(new FileReader(inputFile));
    }

    @Override
    public boolean hasNext() throws IOException {
        readLine = reader.readLine();
        return readLine != null;
    }

    @Override
    public String getRecord() throws IOException {
        return readLine;
    }

    @Override
    public String getHeaders() throws IOException {
        Set<String> headers = new LinkedHashSet<>();
        String inputLine;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)))) {
            while (( inputLine = reader.readLine()) != null) {
                JSONObject jsonObject = new JSONObject(inputLine);
                headers.addAll(jsonObject.keySet());
            }
        }

        return headers.toString().replace("[", " ").replace("]", " ").trim() + "Number";
    }

    @Override
    public String getNumber() throws SQLException {
        return null;
    }

    @Override
    public void close() throws IOException {

    }
}
