package com.croc.edu;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.junit.*;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Дмитрий on 24.06.2015.
 */
public class DummyTest {

    @Before
    public void setUp() {

    }



//    @Test @Ignore
//    public void shouldJunitRun() {
//        DataLoader dataLoader = new DataLoader("D:\\File\\json.txt");
//        DataSaver saver = new DataSaver();
//        JsonParser jsonParser = new JsonParser();
//
//        try {
//            saver.save(jsonParser.parse(dataLoader.read()));
//            assertEquals(1, 2);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Test@Ignore
    public void shouldParseOneStringWhenInputStringInCorrectForm() {
        //region given
        JsonParser jsonParser = new JsonParser();
        String stubString = "{\"PeriodNum\":\"4\",\"Year\":\"2014\",\"ValueType\":\"4\"}##450.0" ;
        //endregion

        //region when
        String result = jsonParser.parse(stubString);
        //endregion

        //region then
        assertEquals(
                "PeriodNum,Year,ValueType\r\n" +
                "4,2014,4,\r\n" ,
            result);
        //endregion
    }

//    @Test@Ignore//(expected = org.json.JSONException.class)
//    public  void shouldParseSomeStringsWhenInputStringIncorrect() throws IOException {
//        //region given
//        JsonParser jsonParser = new JsonParser();
//        DataLoader stubLoader = mock(DataLoader.class);
//        when(stubLoader.read()).thenReturn("PeriodNum,Year,ValueType\n" + "4,2014,4,\r\n");
//        //endregion
//
//        //region when
//        String result = jsonParser.parse(stubLoader.read());
//        //endregion
//
//        //region then
//        assertEquals(
//                "PeriodNum,Year,ValueType\r\n" +
//                        "4,2014,4,\r\n" ,
//                result);
//        //endregion
//
//    }
//    @Test@Ignore
//    public void DataLoaderLoadString() throws IOException {
//        //region given
//       DataLoader dataLoader = new DataLoader("D:\\File\\json.txt");
//        //endregion
//
//        //region when
//        String result = dataLoader.read();
//        //endregion
//
//        //region then
//       // assertEquals("{"PeriodNum":"4"}##450.0", result);
//        //endregion
//    }

    @Test@Ignore
    public void JsonParserInputstringIsNull(){
        //region given
        JsonParser jsonParser = new JsonParser();
        //endregion

        //region when
        try {
            jsonParser.parse(null);
        }
        //endregion

        //region then
        catch (NullPointerException e){
            System.out.println("Input string = null");
        }
        //endregion
    }


}
