package com.croc.edu;

import com.croc.edu.exception.HeadIsNullException;
import com.croc.edu.exception.InputLineException;
import com.croc.edu.parse.JsonParser;
import org.json.JSONException;
import org.junit.Test;

/**
 * Created by Дмитрий on 24.07.2015.
 */
public class ParserTest {
    @Test
    public  void inputLineHaveWrongFormat()  {
        JsonParser jsonParser = new JsonParser();
        try {
        jsonParser.parse("12345","Year");
        }
        catch (JSONException e){
        }
    }

    @Test
    public void headIsNull()  {
        JsonParser jsonParser = new JsonParser();
        try {
            jsonParser.parse("{\"PeriodNum\"}:450",null);
        }
        catch (JSONException e){
        }
    }

    @Test
    public  void  headDoesNotExist() {
        JsonParser jsonParser = new JsonParser();
        try {
            jsonParser.parse("{\"PeriodNum\"}:450","WrongHead");
        }
        catch (JSONException e){
        }
    }

    @Test
    public  void inputLineIsNull(){
        JsonParser jsonParser = new JsonParser();
        try {
            jsonParser.parse(null, "Year");
        }
        catch (JSONException e){
        }
    }
}
