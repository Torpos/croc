package com.croc.edu;

import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * Created by Дмитрий on 09.07.2015.
 */

public class LoaderAPITest {
    @Test
    public void shouldIterateOverResultSet() throws IOException {
        Loader checkedReference;
        try (Loader loader = mock(Loader.class)) {
            checkedReference = loader;
            when(loader.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);

            loader.getHeaders();
            while (loader.hasNext()) {
                loader.getRecord();
            }

            verify(loader).getHeaders();
            verify(loader, times(3)).hasNext();
            verify(loader, times(3)).hasNext();
        }

        verify(checkedReference).close();

    }
}
